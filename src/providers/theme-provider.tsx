"use client";

import { ThemeProvider as MuiThemeProvider, createTheme } from "@mui/material";

export function ThemeProvider({ children }: { children: React.ReactNode }) {
  const theme = createTheme({
    components: {
      MuiTextField: {
        styleOverrides: {
          root: {
            backgroundColor: "#FFFFFF",
            borderTopLeftRadius: "4px",
            borderTopRightRadius: "4px",
          },
        },
        variants: [
          {
            props: { variant: "outlined" },
            style: {
              "& .MuiInputLabel-root": {
                "&.Mui-focused": {
                  color: "white",
                  // backgroundColor: "#013D4F",
                  padding: 6,
                  borderRadius: 8,
                },
              },
            },
          },
        ],
      },

      MuiButton: {
        styleOverrides: {
          root: ({ ownerState }) => ({
            color: "#FFFFFF",
            borderRadius: "24px",
            padding: "15px 16px",

            "&:hover": {
              borderColor: "#999999",
              color: "#FFFFFF",
              backgroundColor:
                ownerState.variant === "outlined" ? "transparent" : "#002935",
            },
          }),

          outlined: {
            border: "1px solid #999999",
          },
        },
      },
    },
    palette: {
      primary: {
        main: "#002935",
      },

      secondary: {
        main: "#023D4F",
        light: "#FFFFFF",
      },
    },
  });

  return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}
