"use client";

// components/SnackbarContext.tsx
import React, {
  createContext,
  useContext,
  useState,
  ReactNode,
  useCallback,
} from "react";
import Snackbar, { SnackbarOrigin } from "@mui/material/Snackbar";
import { Alert, AlertColor } from "@mui/material";

interface SnackbarMessage {
  message: string;
  position: SnackbarOrigin;
  severity: AlertColor;
}

interface ISnackbarContext {
  showMessage: (msg: SnackbarMessage) => void;
}

const SnackbarContext = createContext<ISnackbarContext | undefined>(undefined);

export const useSnackbar = (): ISnackbarContext => {
  const context = useContext(SnackbarContext);
  if (context === undefined) {
    throw new Error("useSnackbar must be used within a SnackbarProvider");
  }
  return context;
};

interface SnackbarProviderProps {
  children: ReactNode;
}

export const SnackbarProvider: React.FC<SnackbarProviderProps> = ({
  children,
}) => {
  const [snackbarInfo, setSnackbarInfo] = useState<
    SnackbarMessage & { open: boolean }
  >({
    message: "",
    position: { vertical: "top", horizontal: "center" },
    open: false,
    severity: "success", // Default value
  });

  const showMessage = useCallback((msg: SnackbarMessage) => {
    setSnackbarInfo({ ...msg, open: true });
  }, []);

  const handleClose = () => {
    setSnackbarInfo((prev) => ({ ...prev, open: false }));
  };

  return (
    <SnackbarContext.Provider value={{ showMessage }}>
      {children}
      <Snackbar
        anchorOrigin={snackbarInfo.position}
        open={snackbarInfo.open}
        onClose={handleClose}
        autoHideDuration={1000}
        key={`${snackbarInfo.position.vertical},${snackbarInfo.position.horizontal}`}
      >
        <Alert
          onClose={handleClose}
          severity={snackbarInfo.severity}
          sx={{ width: "100%" }}
        >
          {snackbarInfo.message}
        </Alert>
      </Snackbar>
    </SnackbarContext.Provider>
  );
};
