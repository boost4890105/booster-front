import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBgliO-D6iUR4RGXUTuUrRVku5MorFwtKw",
    authDomain: "sunlit-almanac-403823.firebaseapp.com",
    projectId: "sunlit-almanac-403823",
    storageBucket: "sunlit-almanac-403823.appspot.com",
    messagingSenderId: "623783252845",
    appId: "1:623783252845:web:67e062ce2c3b655cb88064",
    measurementId: "G-8G1E8FPHYT"
};


const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();

export { auth, provider }