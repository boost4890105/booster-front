import type { Metadata } from "next";
import { ChatInfo } from "@/components/dashboard/chat/sidebar/chat-info";

export const metadata: Metadata = {
  title: "Create Next App",
  description: "Generated by create next app",
};

export default function ChatInfoLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <section className="flex bg-midnightBlue flex-col w-full justify-between">
      <ChatInfo>{children}</ChatInfo>
    </section>
  );
}
