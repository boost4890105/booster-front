import { CiFaceSmile } from "react-icons/ci";
import { MdSentimentNeutral } from "react-icons/md";
import { PiSmileySad } from "react-icons/pi";
import { twMerge } from "tailwind-merge";

export function FeelingEmoji({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  function getEmoji() {
    if (children === "good") {
      return (
        <CiFaceSmile
          className={twMerge("text-2xl text-[#00800093]", className)}
        />
      );
    } else if (children === "bad") {
      return (
        <PiSmileySad
          className={twMerge("text-2xl text-[#ff000098]", className)}
        />
      );
    }
    return (
      <MdSentimentNeutral
        className={twMerge("text-2xl text-neonYellow", className)}
      />
    );
  }

  return <div>{getEmoji()}</div>;
}
