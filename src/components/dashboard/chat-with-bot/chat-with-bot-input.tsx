"use client";

import { AuthContext } from "@/providers/auth-context";
import { useSnackbar } from "@/providers/snack-bar-provider";
import { Input, InputAdornment } from "@mui/material";
import axios from "axios";
import React, { FormEvent, useContext, useState } from "react";
import { LuSendHorizonal } from "react-icons/lu";
import { TiMicrophoneOutline } from "react-icons/ti";

type Props = {
  setTextBotUser: (value: any) => void;
};

export const ChatWithBotInput = (props: Props) => {
  const [chat, setChat] = useState("");
  const { user } = useContext(AuthContext);
  const { showMessage } = useSnackbar();

  const onSubmit = async (e: any) => {
    e.preventDefault();

    props.setTextBotUser((current: any) => [
      ...current,
      {
        user: {
          text: chat,
          date: new Date(),
          imgUrl: user?.photoURL,
          name: user?.displayName,
        },
      },
    ]);
    setChat("");

    try {
      const response = await axios.post("https://backapi.boostlabs.ai/search", {
        query: chat,
      });
      props.setTextBotUser((current: any) => [
        ...current,
        {
          robot: {
            text: response.data.summaryText,
            date: new Date(),
            imgUrl: "/assets/dashboard/logo booster.svg",
            name: "Robot",
          },
        },
      ]);
    } catch {
      showMessage({
        severity: "error",
        message: "Erro ao enviar a mensagem!",
        position: { vertical: "bottom", horizontal: "right" },
      });
    }
  };

  return (
    <form
      onSubmit={onSubmit}
      noValidate
      autoComplete="off"
      className="flex p-8 rounded-md"
    >
      <Input
        className="bg-white w-full p-2 rounded-md"
        placeholder="Enviar mensagem..."
        onChange={(e) => setChat(e.target.value)}
        value={chat}
        startAdornment={
          <button type="submit">
            <InputAdornment position="start">
              <LuSendHorizonal className="text-deepBlue text-4xl" />
            </InputAdornment>
          </button>
        }
        endAdornment={
          <InputAdornment position="end" className="flex gap-1">
            <button>
              <TiMicrophoneOutline className="text-oceanBlue text-4xl" />
            </button>
          </InputAdornment>
        }
      />
    </form>
  );
};
