import Link from "next/link"
import { BsChatText } from "react-icons/bs"

export const ChatWithBotSideBarFilter = () => {
    return (
        <nav className="flex flex-col gap-4 px-4">
            <Link href="/chatwithbot">
                <div className="bg-mediumGray text-white p-2 -ml-2 rounded-md flex gap-3">
                    <BsChatText size={20} />
                    <span>Todas as conversas</span>
                </div>
            </Link>
        </nav>
    )
}