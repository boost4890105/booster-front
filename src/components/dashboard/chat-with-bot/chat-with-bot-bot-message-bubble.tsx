import { Avatar } from "@mui/material";
import Image from "next/image";
import { RiDragDropLine } from "react-icons/ri";

export const ChatWithBotBotMessageBubble = ({
  messageData,
}: {
  messageData: any;
}) => {
  return (
    <div
      key={messageData.date}
      className="my-2 flex flex-col items-start pr-32"
    >
      <div className="flex items-start flex-row-reverse">
        <div className="flex flex-col items-start">
          <div className="rounded p-4 bg-[#D9D9D9] text-black">
            <div className="flex justify-between items-center gap-4">
              <div className="flex gap-3">
                <span className="text-sm">{messageData.name}</span>
                {/* {messageData.showDnDIcon ? <RiDragDropLine className="text-2xl" /> : null} */}
              </div>
              <span className="text-sm">
                {messageData.date.toISOString().substr(11, 8)}
              </span>
            </div>
            <div className="text-base mt-2">{messageData.text}</div>
          </div>
        </div>
        <div className="bg-white mr-2 p-2 rounded-full">
          <Image
            className=""
            src={messageData.imgUrl}
            alt={`${messageData.name} photo`}
            width="40"
            height="40"
          />
        </div>
      </div>
    </div>
  );
};
