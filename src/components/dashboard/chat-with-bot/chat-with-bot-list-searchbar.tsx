import { TextField, InputAdornment, Button } from '@mui/material'
import React from 'react'
import { FiSearch } from 'react-icons/fi'
import LocalOfferOutlinedIcon from '@mui/icons-material/LocalOfferOutlined';
import PushPinOutlinedIcon from '@mui/icons-material/PushPinOutlined';
import CalendarMonthOutlinedIcon from '@mui/icons-material/CalendarMonthOutlined';

export const ChatWithBotListSearchBar = () => {
    return (
        <div className="px-4">
            <TextField
                variant="filled"
                label="Pesquisar"
                type="text"
                className="rounded-lg"
                fullWidth
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <FiSearch className="text-gray-500" />
                        </InputAdornment>
                    ),
                }}
            />
            <div className="text-white flex text-2xl gap-4 py-4">
                <LocalOfferOutlinedIcon />
                <PushPinOutlinedIcon />
                <CalendarMonthOutlinedIcon />
            </div>
            <Button  className="bg-oceanBlue text-sm px-3 rounded-full">+ Nova Conversa</Button>
        </div>
    )
}