"use client";
import { AuthContext } from "@/providers/auth-context";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { BsChatText, BsFillCameraVideoFill, BsHeadset } from "react-icons/bs";
import { GoPeople } from "react-icons/go";
import { IoSettingsOutline } from "react-icons/io5";
import { RiDragDropLine } from "react-icons/ri";
import { useContext } from "react";

export function LeftSideNavbar() {
  const path = usePathname();

  const { user } = useContext(AuthContext);

  return (
    <div className="flex flex-col h-full py-8 w-[120px] justify-between border-r-[3px] border-[#13131386]">
      <nav className="flex gap-8 flex-col text-white items-center">
        {/* to-do: create an array of items and map then in nav.  - to make code smaller and use the same wrapping style*/}
        <Link href="/">
          <Image
            src="/assets/logo.svg"
            alt="Picture of the author"
            width={50}
            height={50}
          />
        </Link>

        <Link href="/">
          <div
            className={`${
              path === "/"
                ? "bg-mediumGray"
                : "hover:bg-[#80808041] transition-all duration-200"
            }  p-3 rounded-full `}
          >
            <BsChatText size={35} />
          </div>
        </Link>
        <Link href="/chatWithBot">
          <div
            className={`${
              path === "/chatWithBot"
                ? "bg-mediumGray"
                : "hover:bg-[#80808041] transition-all duration-200"
            }  p-3 rounded-full `}
          >
            <Image
              src="/assets/dashboard/bot-white.svg"
              alt="Chat with Bot icon"
              width={35}
              height={35}
            />
          </div>
        </Link>
        <Link href="/uploadfiles">
          <div
            className={`${
              path === "/uploadfiles"
                ? "bg-mediumGray"
                : "hover:bg-[#80808041] transition-all duration-200"
            }  p-3 rounded-full `}
          >
            <RiDragDropLine size={35} />
          </div>
        </Link>
        {/* <Link href="/">
          <div
            className={`${path === "/call" ? "bg-mediumGray" : ""
              }  p-3 rounded-full`}
          >
            <BsFillCameraVideoFill size={35} />
          </div>
        </Link>
        <Link href="/">
          <div
            className={`${path === "/comunity" ? "bg-mediumGray" : ""
              }  p-3 rounded-full`}
          >
            <GoPeople size={35} />
          </div>
        </Link> */}
      </nav>

      <nav className="flex gap-8 flex-col text-white items-center">
        {/* <Link href="/">
          <div>
            <BsHeadset size={35} />
          </div>
        </Link> */}

        <Link href="/">
          <div
            className={`${
              path === "/settings"
                ? "bg-mediumGray"
                : "hover:bg-[#80808041] transition-all duration-200"
            }  p-3 rounded-full `}
          >
            <IoSettingsOutline size={35} />
          </div>
        </Link>
        <Link href="/">
          <div>
            <div className="h-[45px] w-[45px] relative">
              <Image
                src={user?.photoURL || "/assets/dashboard/random-man.jpg"}
                alt="Picture of the author"
                layout="fill"
                objectFit="cover"
                className="rounded-full"
              />
              <div className="rounded-full h-4 w-4 bg-[#07FF3E] absolute bottom-0 right-0"></div>
            </div>
          </div>
        </Link>
      </nav>
    </div>
  );
}
