import SentimentDissatisfiedOutlinedIcon from '@mui/icons-material/SentimentDissatisfiedOutlined';

export const ChatAnalysis = () => {
  return (
    <div className='bg-[#AC1F1F] mb-2 block p-2 rounded'>
      <span className='text-white text-sm px-2 py-1'>Análise de sentimento: Negativa</span>
      <SentimentDissatisfiedOutlinedIcon className='text-white'/>
    </div>
  );
}
