import Image from "next/image";
import { Tags } from "../../ui/tags";
import { FeelingEmoji } from "@/components/ui/feeling-emoji";

type Props = {
  name: string;
  image: string;
  alt: string;
  message: string;
  feeling: string;
  tags: string[];
  time: string;
};

export function ChatCard(props: Props) {
  return (
    <button onClick={() => {}} className=" w-full">
      <div className="bg-darkTeal p-3 border-2 border-x-0 border-[#13131386] w-full">
        <div className="flex">
          <div className="h-20 w-20 relative">
            <Image
              src={props.image}
              alt={props.alt}
              fill
              objectFit="cover"
              className="rounded-full"
            />
            <div className="rounded-full h-4 w-4 bg-[#07FF3E] absolute bottom-0 right-0"></div>
          </div>
          <div className="flex flex-col ml-4 flex-1">
            <div className="flex justify-between items-center">
              <div className="flex flex-col">
                <h2 className="text-white font-bold text-base  mb-[2px]">
                  {props.name}
                </h2>
                <p className="text-lightGray w-fit">Você: {props.message}</p>
              </div>
              <div className="flex flex-col items-end gap-2">
                <time className="text-lightGray">{props.time}</time>
                <FeelingEmoji>{props.feeling}</FeelingEmoji>
              </div>
            </div>
            <div className="flex gap-2">
              {props.tags.map((el) => (
                <Tags key={el}>{el}</Tags>
              ))}
            </div>
          </div>
        </div>
      </div>
    </button>
  );
}
